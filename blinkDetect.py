from imutils.video import VideoStream
import cv2
import dlib 
import numpy as np 
import datetime
import time 
import argparse
import imutils
import subprocess
import commands
import math

blinks = 0 
rightStartTime = None
leftStartTime = None
nextTrackScript = """
	tell application "Spotify"
		next track
	end tell
	"""

prevTrackScript = """
	tell application "Spotify"
		previous track
		previous track
	end tell
	"""

def euclideanDistance(point1, point2):
	xdiff = point1[0] - point2[0]
	ydiff = point1[1] - point2[1]
	distance = math.sqrt((xdiff**2) + (ydiff**2))
	return distance

def currentTime():
	return datetime.datetime.now()


def findEyeRatios(coords, frame):
	global blinks, leftStartTime, rightStartTime, nextTrackScript, prevTrackScript
	leftEyePoints = range(36, 42)
	rightEyePoints = range(42, 48)
	for pointNum in rightEyePoints:
		cv2.circle(frame, coords[pointNum], 1, (0, 0, 255), 2)
	for pointNum in leftEyePoints:
		cv2.circle(frame, coords[pointNum], 1, (255, 0, 0), 2)

	rightWidth = euclideanDistance(coords[42], coords[45])
	leftWidth = euclideanDistance(coords[36], coords[39])

	rightHeight1 = euclideanDistance(coords[43], coords[47])
	leftHeight1 = euclideanDistance(coords[37], coords[41])

	rightHeight2 = euclideanDistance(coords[44], coords[46])
	leftHeight2 = euclideanDistance(coords[38], coords[40])

	rightRatio = float((rightHeight1 + rightHeight2)/float(2.0*rightWidth))
	leftRatio = float(float(leftHeight1 + leftHeight2)/float(2.0*leftWidth))
	print("RightRatio: ", rightRatio)
	print("LeftRatio: ", leftRatio)

	#print("RightRatio: ", rightRatio)
	#print("LeftRatio: ", leftRatio)
	if (rightRatio < 0.175 and leftRatio < 0.175):
		#cv2.putText(frame, "BLINK", (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 0), 2, cv2.LINE_AA)
		leftStartTime = None
		rightStartTime = None
		blinks += 1
	elif (leftRatio < 0.18):
		if (rightRatio > 0.14): # 0.145
			#cv2.putText(frame, "Left WINK", (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2, cv2.LINE_AA)
			if leftStartTime == None:
				leftStartTime = currentTime()
				rightStartTime = None 
			else:
				elapsedLeftSeconds = (currentTime() - leftStartTime).total_seconds()
				if elapsedLeftSeconds > 0.5:
					leftStartTime = None
					rightStartTime = None
					cv2.putText(frame, "RIGHT WINK", (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2, cv2.LINE_AA)
					if (len(commands.getoutput('pgrep')) != 3):
						proc = subprocess.Popen(['osascript', '-'], stdin = subprocess.PIPE, stdout = subprocess.PIPE)
						proc.communicate(nextTrackScript)
						print("next Song")
					



	elif (rightRatio < 0.18):
		if (leftRatio > 0.14): # used to be 0.145
			#cv2.putText(frame, "Right WINK", (350, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2, cv2.LINE_AA)
			if rightStartTime == None:
				rightStartTime = currentTime()
				leftStartTime = None
			else:
				elapsedRightSeconds = (currentTime() - rightStartTime).total_seconds
				if elapsedRightSeconds > 0.5:
					leftStartTime = None
					rightStartTime = None
					cv2.putText(frame, "LEFT WINK", (350, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2, cv2.LINE_AA)
					if (len(commands.getoutput('pgrep')) != 3):
						proc = subprocess.Popen(['osascript', '-'], stdin = subprocess.PIPE, stdout = subprocess.PIPE)
						proc.communicate(prevTrackScript)
						print("previous Song")
					


	blinkString = "Blinks: " + str(blinks)
	cv2.putText(frame, blinkString, (frame.shape[0] - 100, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2, cv2.LINE_AA)


def startFaceDetection():
	#initialize camera to use
	cam = 0 
	global blinks
	#intialize dlibs face detector and facial landmark predictor
	faceDetector = dlib.get_frontal_face_detector()
	landmarkPredictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")

	#starting the camera
	camCapture = cv2.VideoCapture(cam)

	if (not camCapture.isOpened()):
		print("The webcam did not start")

	#time.sleep(2)

	while True:
		#capture each frame from the video stream 
		ret, frame = camCapture.read()
		grayFrame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

		rects = faceDetector(grayFrame, 0)

		#draw rectangles around each face
		for i, rect in enumerate(rects):
			point1 = (rect.left(), rect.top())
			point2 = (rect.right(), rect.bottom())
			cv2.rectangle(frame, point1, point2, (0, 255, 0), 3)

		#find the landmark points 
		for rect in rects:
			shape = landmarkPredictor(grayFrame, rect)
			coords = []
			for i in range(0, 68):
				coords.append((shape.part(i).x, shape.part(i).y))
			findEyeRatios(coords, frame)

			"""
			#draw circles around the landmark points and a white one on the tip of the nose
			for point in coords:
				cv2.circle(frame, point, 1, (0, 0, 255), 2)
				if point == coords[33]:
					cv2.circle(frame, point, 1, (255, 255, 255), 6)
			"""


		cv2.imshow('frame', frame)
		#detect when the q keystroke is pressed and break the loop when pressed
		k = cv2.waitKey(2) & 0xFF
		if k == ord('q'):
			break

	camCapture.release()
	cv2.destroyAllWindows()

startFaceDetection()
