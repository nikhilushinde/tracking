from imutils.video import VideoStream
import cv2
import dlib 
import numpy as np 
import datetime
import time 
import argparse
import imutils

def startFaceDetection():
	#initialize camera to use
	cam = 0 
	#intialize dlibs face detector and facial landmark predictor
	faceDetector = dlib.get_frontal_face_detector()
	landmarkPredictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")

	#starting the camera
	camCapture = cv2.VideoCapture(cam)

	if (not camCapture.isOpened()):
		print("The webcam did not start")

	time.sleep(2)

	while True:
		#capture each frame from the video stream 
		ret, frame = camCapture.read()
		grayFrame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

		rects = faceDetector(grayFrame, 0)

		#draw rectangles around each face
		for i, rect in enumerate(rects):
			point1 = (rect.left(), rect.top())
			point2 = (rect.right(), rect.bottom())
			cv2.rectangle(frame, point1, point2, (0, 255, 0), 3)

		#find the landmark points and draw circles in those positions
		for rect in rects:
			shape = landmarkPredictor(grayFrame, rect)
			coords = []
			for i in range(0, 68):
				coords.append((shape.part(i).x, shape.part(i).y))

			for point in coords:
				cv2.circle(frame, point, 1, (0, 0, 255), 2)
				if point == coords[33]:
					cv2.circle(frame, point, 1, (255, 255, 255), 6)

		cv2.imshow('frame', frame)
		#detect when the q keystroke is pressed and break the loop when pressed
		if cv2.waitKey(2) & 0xFF == ord('q'):
			break

	camCapture.release()
	cv2.destroyAllWindows()

startFaceDetection()
