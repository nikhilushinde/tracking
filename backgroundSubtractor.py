import cv2
import numpy as np
import datetime
import time
import dlib

global points, frame, numPoints, detectionMode 
detectionMode = True
histogramMode = False
threshold = 240
everythingElseMask = None
everythingElseHist = None
hand = None
handHist = None
mode = False
points = []
numPoints = 0 

cam = 0 
faceDetector = dlib.get_frontal_face_detector()

def getRectangle(event, x,y, flags, param):
	global points, frame, numPoints
	if detectionMode:
		if event == cv2.EVENT_LBUTTONDOWN:
			if numPoints == 2:
				points = []
				numPoints = 0
			point = (x,y)
			points.append(point)
			numPoints = numPoints + 1


camCapture = cv2.VideoCapture(cam)
cv2.namedWindow("frame")
cv2.setMouseCallback("frame", getRectangle)


while 1:
	ret, frame = camCapture.read()
	frame = cv2.resize(frame, (500, 300), interpolation = cv2.INTER_CUBIC)
	grayFrame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	rects = faceDetector(grayFrame, 0)
	origframe = np.array(frame)

	for i, rect in enumerate(rects):
		point1 = (rect.left(), rect.top())
		point2 = (rect.right(), rect.bottom())
		cv2.rectangle(origframe, point1, point2, (255, 255, 255), -1)

	# draws the points clicked on and a rectangle formed by 2 points if two are clicked
	if detectionMode:
		for point in points:
			cv2.circle(frame,point,5,(0,0,255),1)
		if (len(points) == 2):
			cv2.rectangle(frame, points[0], points[1], (0,255,0),2)

	cv2.imshow("frame", frame)

	# does the histogram backprojection with the calculated histogram
	# then convolves with a disc kernel, thresholds and subtracts backprojection of background
	# finally does a median blur to try to get rid of salt pepper noise in background
	# does an erode operation to try to get rid of excess details
	# displays this in a window labeled "histogramBackprojection"
	if histogramMode:
		histogramFrame = cv2.cvtColor(origframe, cv2.COLOR_BGR2HSV)
		histogramFrame = cv2.calcBackProject([histogramFrame], [0, 1], handHist, [0, 256, 0, 256], 1)
		discKernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (7,7))
		cv2.filter2D(histogramFrame, -1, discKernel, histogramFrame)
		ret, histogramFrame = cv2.threshold(histogramFrame, threshold, 257, 0)

		everythingElseFrame = cv2.cvtColor(origframe, cv2.COLOR_BGR2HSV)
		everythingElseFrame = cv2.calcBackProject([everythingElseFrame], [0, 1], everythingElseHist, [0, 256, 0, 256], 1)
		cv2.filter2D(everythingElseFrame, -1, discKernel, everythingElseFrame)
		ret, everythingElseFrame = cv2.threshold(everythingElseFrame, 180, 257, 0)


		#cv2.imshow("everythingBackprojection", everythingElseFrame)
		histogramFrame = cv2.subtract(histogramFrame, everythingElseFrame)
		cv2.medianBlur(histogramFrame, 9) # increased medianBlur from 7 to 9, 
		cv2.erode(histogramFrame, discKernel, iterations = 3)
		

		# finds the contours and keeps the largest one in largestContour
		_, contours, _ = cv2.findContours(np.array(histogramFrame), cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)
		largestContour = contours
		largestArea = 0 
		for contour in contours:
			area = cv2.contourArea(contour)
			if area>largestArea:
				largestContour = contour
				largestArea = area
		if len(contours) > 0:
			# finds the convex hull around the contour
			# returnPoints = False returns makes it return the placement of the point instead of the point itself
			hull = cv2.convexHull(largestContour, returnPoints = False)
			#hull = cv2.convexHull(largestContour)
			cv2.drawContours(frame, largestContour, -1, (0,255,0), 2)
			#cv2.drawContours(frame, hull, -1, (0, 0, 255), 2)

			# finds the convexity defects 
			defects = cv2.convexityDefects(largestContour, hull)
			# draws lines to form the convex hull and circles at the convex defects 
			"""
			for i in range(defects.shape[0]):
				s, e, f, d = defects[i, 0]
				start = tuple(largestContour[s][0])
				end = tuple(largestContour[e][0])
				farthest = tuple(largestContour[f][0])
				cv2.line(frame, start, end, (0, 0, 255), 3)
				cv2.circle(frame, farthest, 5, (255, 0, 0), -1)
			"""

			for i in range(defects.shape[0]):
				s, e, f, d = defects[i, 0]
				p1 = tuple(largestContour[s][0])
				p2 = tuple(largestContour[e][0])
				p3 = tuple(largestContour[f][0])
				#cv2.circle(frame, p1, 5, (255, 0, 0), -1)
				#cv2.circle(frame, p2, 5, (255, 255, 255), -1)
				cv2.circle(frame, p3, 5, (0, 0, 255), -1)


		cv2.imshow("frame", frame)
		cv2.imshow("histogramBackprojection", histogramFrame)

	# quits the program
	k = cv2.waitKey(2) & 0xFF
	if k == ord("q"):
		break
	if k == 27 & detectionMode:
		points = []
		numPoints = 0

	# toggles detectionMode 
	if k == ord("d"):
		detectionMode = not detectionMode
		points = []
		numPoints = 0 

	# takes the enclosed segment from the detectionMode and calculates a histogram from it to use for histogramBackprojection
	if k == ord("c"):
		print("hand histogramed")
		if numPoints == 2:
			startX = points[0][0]
			startY = points[0][1]
			endX = points[1][0]
			endY = points[1][1]
			startPoint = (min(startX, endX), min(startY, endY))
			endPoint = (max(startX, endX), max(startY, endY))
			everythingElse = np.array(origframe)
			everythingElse[startPoint[1]:endPoint[1], startPoint[0]:endPoint[0]] = 1
			
			hand = origframe[startPoint[1]:endPoint[1], startPoint[0]:endPoint[0]]
			hand = cv2.cvtColor(hand, cv2.COLOR_BGR2HSV)
			handHist = cv2.calcHist([hand], [0,1], None, [256, 256], [0, 256, 0, 256])

			everythingElse = cv2.cvtColor(everythingElse, cv2.COLOR_BGR2HSV)
			everythingElseHist = cv2.calcHist([everythingElse], [0,1], None, [256, 256], [0, 256, 0, 256])

			cv2.normalize(everythingElseHist,everythingElseHist,0,255,cv2.NORM_MINMAX)
			cv2.normalize(handHist,handHist,0,255,cv2.NORM_MINMAX)
			histogramMode = True
			

	if k == ord("e"):
		# only closes histogram window if histogramMode was enabled and the window was opened
		if histogramMode & (cv2.getWindowProperty("histogramBackprojection", 0) >= 0):
			# only destroys the histogramBackprojection window
			cv2.destroyWindow("histogramBackprojection")
			cv2.destroyWindow("everythingBackprojection")
	if k == ord("i"):
		threshold = threshold - 5
		print threshold
	if k == ord("o"):
		threshold = threshold + 5
		print threshold

		histogramMode = not histogramMode
	

camCapture.release()
cv2.destroyAllWindows()