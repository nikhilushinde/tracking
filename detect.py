import cv2
import numpy as np
import datetime 
import time
import dlib

def backgroundSubtractor():
	cam = 0 
	camCapture = cv2.VideoCapture(cam)

	if (not camCapture.isOpened()):
		print("camera did not start")

	time.sleep(2)
	#to do opening on the image
	kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3,3))
	#backgroundSubtractor = cv2.bgsegm.createBackgroundSubtractorGMG()
	backgroundSubtractor = cv2.bgsegm.createBackgroundSubtractorGMG()


	while True:
		ret, frame = camCapture.read()
		foregroundMask = backgroundSubtractor.apply(frame)
		
		foregroundMask =cv2.erode(foregroundMask, kernel, iterations = 2)
		foregroundMask =cv2.dilate(foregroundMask, kernel, iterations = 1)
		foregroundMask = cv2.medianBlur(foregroundMask, 5)
		_, contours, hierarchy = cv2.findContours(foregroundMask,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
		#print(len(contours))
		cv2.drawContours(foregroundMask, contours, -1, (0,0, 255), 10)
		#foregroundMask = cv2.Canny(foregroundMask, 100, 200)

		

		cv2.imshow("foreground", foregroundMask)
		if cv2.waitKey(2) & 0xFF == ord("q"):
			break

	camCapture.release()
	cv2.destroyAllWindows()

	print("exited")



def hsvIsolator():
	cam = 0 
	camCapture = cv2.VideoCapture(cam)

	if (not camCapture.isOpened()):
		print("the camera failed try again")

	time.sleep(2)

	faceDetector = dlib.get_frontal_face_detector()
	detectionMode = False

	lower = np.array([0, 58, 50])
	upper = np.array([60, 173, 255])
	kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (4,4))

	while True:
		ret, frame = camCapture.read()
		hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

		mask = cv2.inRange(hsv, lower, upper)
		mask = cv2.erode(mask, kernel, iterations = 1)
		mask = cv2.dilate(mask, kernel, iterations = 1)
		#frame = cv2.bitwise_and(frame, frame, mask = mask)
		frame = mask

		if detectionMode:
			
			grayFrame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
			rects = faceDetector(grayFrame)
			landmarkPredictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")

			# draw rectangle around face
			for i, rect in enumerate(rects):
				point1 = (rect.left(), rect.top())
				point2 = (rect.right(), rect.bottom())
				cv2.rectangle(frame, point1, point2, (0, 255, 0), 3)

			for rect in rects:
				shape = landmarkPredictor(grayFrame, rect)
				coords = []
				for i in range(0, 68):
					coords.append((shape.part(i).x, shape.part(i).y))

				for point in coords:
					cv2.circle(frame, point, 1, (0, 0, 255), 2)
					if point == coords[33]:
						rgbValueNose = [point]
						print("Nose", rgbValueNose)
						#cv2.circle(frame, point, 1, (255, 255, 255), 6)

					if point == coords[56]:
						rgbValueHand = point
						print("Hand", rgbValueHand)
						#cv2.circle(frame, point, 1, (0, 255, 0), 6)
		

		cv2.imshow("frame", frame)

		if cv2.waitKey(2) & 0xFF == ord("q"):
			break
	cv2.destroyAllWindows()



def hsvFilterTest():
	image = cv2.imread("nikhil.jpg")
	hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

	lower = np.array([0, 58, 50])
	upper = np.array([60, 173, 255])
	mask = cv2.inRange(hsv, lower, upper)

	while 1:
		cv2.imshow("mask", mask)
		if cv2.waitKey(2) & 0xFF == ord("q"):
			break

backgroundSubtractor()

