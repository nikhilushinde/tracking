from imutils.video import VideoStream
import cv2
import dlib 
import numpy as np 
import datetime
import time 
import argparse
import imutils
import subprocess
import commands
import math

blinks = 0 
closedCalibrationMode = False
openCalibrationmode = False

rightClosedEye = 0
rightCount = 0 
rightOpenEye = 0 

leftClosedEye = 0
leftCount = 0 
leftOpenEye = 0 


def euclideanDistance(point1, point2):
	xdiff = point1[0] - point2[0]
	ydiff = point1[1] - point2[1]
	distance = math.sqrt((xdiff**2) + (ydiff**2))
	return distance

def currentTime():
	return datetime.datetime.now()


def findEyeRatios(coords, frame):
	global blinks, leftStartTime, rightStartTime, nextTrackScript, prevTrackScript
	leftEyePoints = range(36, 42)
	rightEyePoints = range(42, 48)
	for pointNum in rightEyePoints:
		cv2.circle(frame, coords[pointNum], 1, (0, 0, 255), 2)
	for pointNum in leftEyePoints:
		cv2.circle(frame, coords[pointNum], 1, (255, 0, 0), 2)

	rightWidth = euclideanDistance(coords[42], coords[45])
	leftWidth = euclideanDistance(coords[36], coords[39])

	rightHeight1 = euclideanDistance(coords[43], coords[47])
	leftHeight1 = euclideanDistance(coords[37], coords[41])

	rightHeight2 = euclideanDistance(coords[44], coords[46])
	leftHeight2 = euclideanDistance(coords[38], coords[40])

	rightRatio = float((rightHeight1 + rightHeight2)/float(2.0*rightWidth))
	leftRatio = float(float(leftHeight1 + leftHeight2)/float(2.0*leftWidth))
	print("RightRatio: ", rightRatio)
	print("LeftRatio: ", leftRatio)

	if closedCalibrationMode:
		rightCount = rightCount + 1
		leftCount = leftCount + 1
		rightClosedEye = rightClosedEye + rightRatio
		leftClosedEye = leftClosedEye + leftRatio

	elif openCalibrationmode:
		rightCount = rightCount + 1
		leftCount = leftCount + 1
		rightOpenEye = rightOpenEye + rightRatio
		leftOpenEye = leftOpenEye + leftRatio


	blinkString = "Blinks: " + str(blinks)
	cv2.putText(frame, blinkString, (frame.shape[0] - 100, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2, cv2.LINE_AA)


def startFaceDetection():
	#initialize camera to use
	cam = 0 
	global blinks
	#intialize dlibs face detector and facial landmark predictor
	faceDetector = dlib.get_frontal_face_detector()
	landmarkPredictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")

	#starting the camera
	camCapture = cv2.VideoCapture(cam)

	if (not camCapture.isOpened()):
		print("The webcam did not start")

	#time.sleep(2)

	while True:
		#capture each frame from the video stream 
		ret, frame = camCapture.read()
		grayFrame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

		rects = faceDetector(grayFrame, 0)

		#draw rectangles around each face
		for i, rect in enumerate(rects):
			point1 = (rect.left(), rect.top())
			point2 = (rect.right(), rect.bottom())
			cv2.rectangle(frame, point1, point2, (0, 255, 0), 3)

		#find the landmark points 
		for rect in rects:
			shape = landmarkPredictor(grayFrame, rect)
			coords = []
			for i in range(0, 68):
				coords.append((shape.part(i).x, shape.part(i).y))
			findEyeRatios(coords, frame)

			"""
			#draw circles around the landmark points and a white one on the tip of the nose
			for point in coords:
				cv2.circle(frame, point, 1, (0, 0, 255), 2)
				if point == coords[33]:
					cv2.circle(frame, point, 1, (255, 255, 255), 6)
			"""


		cv2.imshow('frame', frame)
		#detect when the q keystroke is pressed and break the loop when pressed
		k = cv2.waitKey(2) & 0xFF
		if k == ord('q'):
			break

		if k == ord('c'):
			if closedCalibrationMode:
				closedCalibrationMode = not closedCalibrationMode


	camCapture.release()
	cv2.destroyAllWindows()

startFaceDetection()
